//
//  ViewController.h
//  CT
//
//  Created by David Johnson on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCPad_sub.h"

#define EVENT_APP_ENTERED_BACKGROUND @"EVENT_APP_ENTERED_BACKGROUND"
#define KEY_SCREEN @"KEY_SCREEN"
#define KEY_USE_ANIM @"KEY_USE_ANIM"

@interface ViewController : UIViewController {
    VCPad_sub *screen;
    UINavigationController *nav;
    NSDictionary *screenDict;
}

- (IBAction)advance:(id)sender;

@end
