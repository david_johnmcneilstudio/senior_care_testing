//
//  VCPad_sub.h
//  CT
//
//  Created by David Johnson on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define EVENT_ADVANCE_SCREEN @"EVENT_ADVANCE_SCREEN"
#define KEY_ADVANCE_SCREEN @"KEY_ADVANCE_SCREEN"
#define KEY_TAG @"KEY_TAG"

@interface VCPad_sub : UIViewController {
    int index;
}


- (IBAction)advance:(id)sender;

- (void) setIndex:(int) index;

@end
