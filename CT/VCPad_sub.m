//
//  VCPad_sub.m
//  CT
//
//  Created by David Johnson on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "VCPad_sub.h"

@interface VCPad_sub ()

@end

@implementation VCPad_sub

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (IBAction)advance:(id)sender {
    NSLog(@"advance");
    NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:index], KEY_ADVANCE_SCREEN, [NSNumber numberWithInt:[sender tag]], KEY_TAG, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:EVENT_ADVANCE_SCREEN object:self userInfo:info];
}

- (void) setIndex:(int) newIndex {
    index = newIndex;
}


@end
