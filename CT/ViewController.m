//
//  ViewController.m
//  CT
//
//  Created by David Johnson on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "VCPad_sub.h"

@interface ViewController ()

- (void) onAdvance:(NSNotification *) note;
- (void) onAppEnteredBackground: (id) sender;
- (void) showNextPadScreen:(int)index forScreen:(UIViewController *)oldScreen;
- (void) showNextPhoneScreen:(int) screenNumber 
                useAnimation:(int) useAnim
                   forScreen:(UIViewController *)oldScreen;
- (void) createScreens;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self 
           selector:@selector(onAdvance:) 
               name:EVENT_ADVANCE_SCREEN 
             object:nil];
    
    [nc addObserver:self
           selector:@selector(onAppEnteredBackground:)
               name:EVENT_APP_ENTERED_BACKGROUND
             object:nil];
    
    [self createScreens];
    
}

- (void) createScreens {
    screenDict = [[NSMutableDictionary alloc] init];
    
    NSNumber *screenIgnore = [NSNumber numberWithInt:0];
    NSNumber *screen1 = [NSNumber numberWithInt:1];
    NSNumber *screen2 = [NSNumber numberWithInt:2];
    NSNumber *screen3 = [NSNumber numberWithInt:3];
    NSNumber *screen4 = [NSNumber numberWithInt:4];
    NSNumber *screen5 = [NSNumber numberWithInt:5];
    NSNumber *screen6 = [NSNumber numberWithInt:6];
    NSNumber *screen7 = [NSNumber numberWithInt:7];
    NSNumber *screen8 = [NSNumber numberWithInt:8];
    NSNumber *screen9 = [NSNumber numberWithInt:9];
    NSNumber *screen10 = [NSNumber numberWithInt:10];
    NSNumber *screen11 = [NSNumber numberWithInt:11];
    NSNumber *screen12 = [NSNumber numberWithInt:12];
    NSNumber *screen13 = [NSNumber numberWithInt:13];
    NSNumber *screen14 = [NSNumber numberWithInt:14];
    NSNumber *screen15 = [NSNumber numberWithInt:15];
    NSNumber *screen16 = [NSNumber numberWithInt:16];
    
    NSNumber *useAnim = [NSNumber numberWithInt:1];
    NSNumber *noAnim = [NSNumber numberWithInt:0];
    
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0", nil]
                  forKey:@"0"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen6, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen5, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen2, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"2", nil]
                  forKey:@"1"]; 
     
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen6, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1", nil]
                  forKey:@"2"];
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen6, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen5, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen4, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screenIgnore, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"3"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen6, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen5, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen4, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen3, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"4"];  
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen6, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen5, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen4, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen3, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"5"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen7, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen5, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"2", nil]
                  forKey:@"6"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen10, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen12, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen11, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screenIgnore, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"4",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen8, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"5", nil]
                  forKey:@"7"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen10, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen12, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen11, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen9, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"8"]; 
    
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen10, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen12, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen11, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen7, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"4",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen8, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"5", nil]
                  forKey:@"9"]; 
    
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screenIgnore, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen16, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen9, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen6, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"10"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen10, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen12, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen4, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen7, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"11"];  
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen10, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"1",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen5, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"2",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen11, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"3",
      [[NSDictionary alloc] initWithObjectsAndKeys:screen7, KEY_SCREEN, noAnim, KEY_USE_ANIM, nil], @"4", nil]
                  forKey:@"12"]; 
    
    [screenDict setValue:
     [[NSDictionary alloc] initWithObjectsAndKeys:
      [[NSDictionary alloc] initWithObjectsAndKeys:screen1, KEY_SCREEN, useAnim, KEY_USE_ANIM, nil], @"0", nil]
                  forKey:@"16"]; 
    
}

- (void) onAppEnteredBackground: (id) sender {
    //    NSLog(@"It worked! screenCounter: %d, thisIndex: %d", screenCounter, thisIndex);
    
    [self dismissModalViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


- (IBAction)advance:(id)sender {
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        // [self showNextPadScreen:0 forScreen:self];
    } else {
        [self showNextPhoneScreen:1 useAnimation:1 forScreen:self];
    }
    
}

- (void) onAdvance:(NSNotification *) note { 
    
    int oldScreenNum = [(NSNumber *)[[note userInfo] valueForKey:KEY_ADVANCE_SCREEN] intValue];
    int btnNum = [(NSNumber *)[[note userInfo] valueForKey:KEY_TAG] intValue];
    UIViewController *oldScreen = (UIViewController *)[note object];
    
    NSDictionary *screenData = [screenDict objectForKey:[NSString stringWithFormat:@"%d", oldScreenNum]];
    NSDictionary *button = [screenData objectForKey:[NSString stringWithFormat:@"%d", btnNum]];
    int newScreenNum = [[button objectForKey:KEY_SCREEN] intValue];
    int useAnim = [[button objectForKey:KEY_USE_ANIM] intValue];
    
    NSLog(@"onAdvance(): gotoScreen: %d, fromScreen: %d, useAnim: %d", newScreenNum, oldScreenNum, useAnim);
    
    if( newScreenNum == oldScreenNum ) 
        return;
    if( newScreenNum == 0 ) 
            return;
    
    if( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        // [self showNextPadScreen:index forScreen:oldScreen];
    } else {
        [self showNextPhoneScreen:newScreenNum useAnimation:useAnim forScreen:oldScreen];
    }
}

- (void) showNextPhoneScreen:(int) screenNumber 
                useAnimation: (int) useAnim
                   forScreen:(UIViewController *)oldScreen {
    
    NSString *screenName = [NSString stringWithFormat:@"View_Phone%d", screenNumber];
    NSLog(@"showNextPhoneScreen() screenNumber: %d, useAnim: %d, screenName: %@", screenNumber, useAnim, screenName);
    screen = [[VCPad_sub alloc] initWithNibName:screenName bundle:nil];
    [screen setIndex:screenNumber];
    [oldScreen presentModalViewController:screen animated:(useAnim == 1 ? YES : NO)];
    
}

//- (void) showNextPadScreen:(int) index
//                 forScreen:(UIViewController *)oldScreen {
//    
//    if( index == 0 ) {   
//        screen = [[VCPad_sub alloc] initWithNibName:@"View_Pad1" bundle:nil];
//        [screen setIndex:1];
//        [oldScreen presentModalViewController:screen animated:YES];
//        
//    } else if ( index == 1 ) {
//        screen = [[VCPad_sub alloc] initWithNibName:@"View_Pad2" bundle:nil];
//        [screen setIndex:2];
//        nav = [[UINavigationController alloc] initWithRootViewController:screen];
//        [nav setNavigationBarHidden:YES];
//        
//        [oldScreen presentModalViewController:nav animated:YES];
//        
//    } else if ( index == 2 ) {
//        screen = [[VCPad_sub alloc] initWithNibName:@"View_Pad3" bundle:nil];
//        [nav pushViewController:screen animated:YES];
//    }
//
//}




@end
